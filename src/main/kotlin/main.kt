fun main() {
    println(numToString(4789)) //Tells the user to put in numbers from 1 to 100
    println(numToString(527))
    println(numToString(28))
    println(numToString(11))
    println(numToString(7))
    println(numToString(99))
    println(numToString(50)) // Prints the amount of points i'd like to get:)
}

// numToString function is created for converting numbers into words. number is passed as an argument.

fun numToString(number: Int): String {
    // Firstly, I made 4 arrays for words I would need in the program. After that, I counted the length of the number.
    val ones = arrayOf("", "ერთი", "ორი", "სამი", "ოთხი", "ხუთი", "ექვსი", "შვიდი", "რვა", "ცხრა")
    val teens = arrayOf(
        "", "თერთმეტი", "თორმეტი", "ცამეტი", "თოთხმეტი", "თხუთმეტი", "თექვსმეტი", "ჩვიდმეტი",
        "თვრამეტი", "ცხრამეტი"
    )
    val tens = arrayOf(
        "", "ათი", "ოცი", "ოცდაათი", "ორმოცი", "ორმოცდაათი", "სამოცი", "სამოცდაათი", "ოთხმოცი",
        "ოთხმოცდაათი"
    )
    val hundreds = arrayOf("", "ასი", "ორასი", "სამასი", "ოთხასი", "ხუთასი", "ექვსასი", "შვიდასი", "რვაასი", "ცხრაასი")
    val len = number.toString().length
    // Program checks the length, If the number has only one digit, it finds the digit in the compatible array.
    if (len == 1) {
        return (ones[number])
        /* If the number has two digits and is from 11-19, program finds the word for that number in an array by the last
         * digit of the number.*/
    } else if (len == 2 && number % 10 != 0) {
        if (number / 10 == 1) {
            return (teens[number % 10])
            /* However, if number is even and has two digits, program finds the word for tens and ones
             * of the number in separate arrays. Tens are found by chopping off the last digit of the number by dividing
             * it by ten, then using that as an index, after desired value is returned from the array I replace 'ი' with
             * 'და' so that number will be spelled correctly. Ones if found by finding the remainder of the number
             * and using it as an index.*/
        } else if (number / 10 == 2 || number / 10 == 4 || number / 10 == 6 || number / 10 == 8) {
            val tensValue = tens[number / 10].replace("ი", "და")
            return ("${tensValue}${ones[number % 10]}")
            // Similar process goes when the number is odd, but in this case 'ათი' is replaced by empty character.
        } else if (number / 10 == 3 || number / 10 == 5 || number / 10 == 7 || number / 10 == 9) {
            val tensValue = tens[number / 10].replace("ათი", "")
            return ("${tensValue}${teens[number % 10]}")
        }
        // Then its determined whether number divides by ten without any reminders and its easily found in tens array.
    } else if (len == 2 && number % 10 == 0) {
        return (tens[number / 10])
        /* Afterwards, the program checks numbers with three digits. I used substring() function tu get rid of every 'ი' in
         * words from hundreds array. */
    } else if (len == 3 && number % 100 != 0) {
        val hundredsIndex = hundreds[number / 100]
        val hundredsValue = hundredsIndex.substring(0, hundredsIndex.length - 1)
        // If the number is divided by 10 without reminders, similar process goes like before.
        if (number % 10 == 0) {
            val tensIndex = number % 100
            val tensValue = tens[tensIndex / 10]
            return ("$hundredsValue $tensValue")
            // However, if that's not the case, I use recursion and find the tens and ones like that.
        } else if (number % 10 != 0) {
            val tensValue = numToString(number % 100)
            return ("$hundredsValue $tensValue")
        }
        // This part of the code returns hundreds.
    } else if (len == 3 && number % 100 == 0) {
        return (hundreds[number / 100])
        // This one returns a thousand.
    } else if (number == 1000) {
        return "ათასი"
        // This one tells the user to input numbers only from 1 to 1000.
    } else if (number > 1000 || len == 0) {
        return "შეიყვანეთ რიცხვები 1-დან 1000მდე"
    }

    return "$number"
}